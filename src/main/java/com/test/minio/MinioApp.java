package com.test.minio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * minio应用
 * 
 * @date 2022-05-31 09:52
 * @version 1.0
 */
@SpringBootApplication
public class MinioApp {

	public static void main(String[] args) {
		SpringApplication.run(MinioApp.class, args);
	}
}
