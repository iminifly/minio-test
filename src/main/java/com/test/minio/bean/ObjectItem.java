package com.test.minio.bean;

import lombok.Data;

/**
 * 对象信息
 * 
 * @Copyright (c) 北京吉威空间信息股份有限公司.All Rights Reserved.
 * @link http://www.geoway.com.cn
 * @author 吕晓飞
 * @date 2022-05-31 11:28
 * @version 1.0
 */
@Data
public class ObjectItem {
	private String objectName;
	private Long size;
}
