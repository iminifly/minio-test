package com.test.minio.config;

import javax.annotation.Resource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.test.minio.prop.MinIoClientProp;

import io.minio.MinioClient;

/**
 * minio客户端
 * 
 * @date 2022-05-31 10:16
 * @version 1.0
 */
@Configuration
public class MinioConfig {

	@Resource
	private MinIoClientProp clientProp;

	/**
	 * 创建客户端
	 * 
	 * @return
	 */
	@Bean
	public MinioClient minioClient() {
		return MinioClient.builder()//
				.endpoint(clientProp.getEndpoint())//
				.credentials(clientProp.getAccessKey(), clientProp.getSecretKey())//
				.build();
	}
}
