package com.test.minio.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

/**
 * 在线api框架配置
 * 
 * @since 2019-08-15 16:44
 * @version 1.0
 */
@Configuration
@EnableOpenApi
public class Swagger2Config {
	@Bean
	public Docket createRestApi() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select()
				// .apis(RequestHandlerSelectors.basePackage("com.geoway.rsrd.modules.data.controller"))
				.apis(RequestHandlerSelectors.withClassAnnotation(RestController.class)).paths(PathSelectors.any())
				.build();
	}

	private ApiInfo apiInfo() {
		Contact contact = new Contact("吕晓飞", "http://www.geoway.com.cn", "lvxiaofei@geoway.com.cn");
		return new ApiInfoBuilder()//
				.title("minio测试服务文档")//
				.description("minio在线测试api")//
				.contact(contact)//
				.termsOfServiceUrl("http://www.geoway.com.cn")//
				.version("0.0.1-SNAPSHOT")//
				.build();
	}

}
