package com.test.minio.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.test.minio.prop.MinIoClientProp;
import com.test.minio.utils.MinioUtil;

import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/test")
public class TestCtrl {

	@Resource
	private MinIoClientProp clientProp;

	@Resource
	private MinioUtil minioUtil;

	@PostMapping("/upload")

	public ResponseEntity<String> upload(@RequestPart MultipartFile file) {
		String endpoint = clientProp.getEndpoint();
		String defaultBucketName = clientProp.getDefaultBucketName();
		List<String> upload = minioUtil.upload(defaultBucketName, file);
		return ResponseEntity.ok(StrUtil.format("{}/{}/{}", endpoint, defaultBucketName, upload.get(0)));
	}
}
