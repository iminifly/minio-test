package com.test.minio.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Data;

/**
 * minio客户端连接配置
 * 
 * @date 2022-05-31 10:12
 * @version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "minio")
public class MinIoClientProp {

	/**
	 * minio服务地址
	 */
	String endpoint;

	/**
	 * 用户名
	 */
	String accessKey;

	/**
	 * 密码
	 */
	String secretKey;

	/**
	 * 默认存储桶名称
	 */
	String defaultBucketName;
}
